﻿using System;

namespace DocHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            DocReader reader = new DocReader("D:\\test");
            try
            {
                //reader.MakeFileCopy_usingStreamWriter();
                //reader.MakeFileCopy_usingFile();
                //reader.MakeFileCopyUsing_BinaryReader();
                //reader.MakeFileCopy_usingStreamWriter_2();
                //reader.MakeCopyUsing_Base64();
                reader.MakeCopyInChunks();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}