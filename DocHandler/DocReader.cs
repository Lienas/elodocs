﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DocHandler
{
    public class DocReader
    {
        // File Info Object of the pdf File
        private FileInfo _fileInfo;

        public DocReader(string pathToFile)
        {
            this._fileInfo = ReadFileFromFolder(pathToFile);
        }

        /**
         * Helper Function to get a pdf for testing
         * Reading from D:/Test
         * File must start with an S
         */
        private static FileInfo ReadFileFromFolder(string pathToFile)
        {
            var directory = new DirectoryInfo(pathToFile);
            try
            {
                var list = directory.GetFiles("S*.pdf");
                if (list.Length > 0)
                {
                    return list[0];
                }

                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new FileNotFoundException("No pdf File in " + pathToFile);
            }
        }


        /**
         *  This is just a "quick Test" of copying a pdf using chars
         *  Strategy:
         *  Read the file into bytes-Array
         *  Convert to BASE 64 encoded chars
         *  Convert back from char-Array to bytes-Array
         *  Writes bytes to disk
         */
        public void MakeCopyUsing_Base64()
        {
            var copyName = _fileInfo.DirectoryName + "\\file_copy_3.pdf";

            byte[] bytes;
            bytes = File.ReadAllBytes(_fileInfo.FullName);

            //Encode to BASE64
            // calculate the size needed for encoding
            var outArrayLength = (long) ((4.0d / 3.0d) * bytes.Length);
            if (outArrayLength % 4 != 0)
            {
                outArrayLength += 4 - outArrayLength % 4;
            }

            // prepare outArray
            var outArray = new char[outArrayLength];

            //prepare chunks
            var base64Converted = Convert.ToBase64CharArray(bytes, 0, bytes.Length, outArray, 0);
            var bytesDecoded = Convert.FromBase64CharArray(outArray, 0, Convert.ToInt32(outArrayLength));

            Console.WriteLine("Write {0}", copyName);
            File.WriteAllBytes(copyName, bytesDecoded);
            Console.WriteLine("Finished Doc has {0} bytes", bytes.Length);
        }

        /**
         * final Solution splitted in
         * public method
         * two private helper methods
         */
        public void MakeCopyInChunks()
        {
            // set name of copy
            var copyName = _fileInfo.DirectoryName + "\\file_copy_chunked.pdf";

            //prepare a filestream
            FileStream fs = null;

            try
            {
                // get filestream
                fs = new FileStream(_fileInfo.FullName, FileMode.Open);

                // calculate the resulting lines
                var chunksCount = (int) Math.Ceiling(_fileInfo.Length / 1024.0);

                // prepare arrays
                var lines = new string[chunksCount];
                var bytes = new byte[1024];

                // Read in chunks
                for (var chunk = 0; chunk < chunksCount; chunk++)
                {
                    // read 1024 bytes an move pointer
                    fs.Read(bytes, 0, 1024);

                    //convert 1 line to a Base64 encoded string
                    lines[chunk] = Read_Base64_Chunks(bytes);

                    // Write the string to console
                    Console.WriteLine("Chunk {0}: {1}", chunk, lines[chunk]);
                }

                //reassemble 
                // call helper method to reconvert the encoded stings to bytes
                var reassembled = Write_Base64_Chunks(lines);

                //finally write bytes to disk
                File.WriteAllBytes(copyName, reassembled);
                Console.WriteLine("Successfully wrote file: {0}", copyName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                fs?.Close();
            }
        }

        /// <summary>
        /// Converts the byte-Array to a Base64 Encoded String
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        private string Read_Base64_Chunks(byte[] byteArray)
        {
            //calculate Size for base64EncodedArray
            var base64EncodedArraySize = (long) ((4.0d / 3.0d) * byteArray.Length);
            //if not divide-able by 4 round up
            if (base64EncodedArraySize % 4 != 0)
            {
                base64EncodedArraySize += 4 - base64EncodedArraySize % 4;
            }

            //prepare base64EncodedArray
            var base64EncodedArray = new char[base64EncodedArraySize];

            //Convert
            var returnedSizeOfBase64EncodedArray =
                Convert.ToBase64CharArray(byteArray, 0, byteArray.Length, base64EncodedArray, 0);

            Console.WriteLine("{0} bytes written to array -calculated {1}",
                returnedSizeOfBase64EncodedArray, base64EncodedArraySize);

            return new string(base64EncodedArray);
        }

        /// <summary>
        /// Convert Collection of Strings to an Byte Array
        /// </summary>
        /// <param name="base64EncodedStringArray">Collection of strings</param>
        /// <returns></returns>
        private static byte[] Write_Base64_Chunks(IEnumerable<string> base64EncodedStringArray)
        {
            //prepare reassembled Bytes
            Console.WriteLine("Prepare Reassembling");
            var reassembled = new List<byte>();

            foreach (var base64EncodedString in base64EncodedStringArray)
            {
                //Decode to bytes 
                var bytes = Convert.FromBase64String(base64EncodedString);
                foreach (var b in bytes)
                {
                    reassembled.Add(b);
                }
            }

            return reassembled.ToArray();
        }
    }
}